An absolute bare-bones web app.

Created from templates made available by Stagehand under a BSD-style
[license](https://github.com/dart-lang/stagehand/blob/master/LICENSE).

# Instalando o package do servidor web
pub global activate webdev

# Instalando o gerenciador scaffolding
 pub global activate stagehand