class ToDo {
  final int id;
  final int userId;
  final String name;
  final bool done;

  ToDo({this.userId, this.id, this.done, this.name});


}
