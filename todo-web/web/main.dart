import 'dart:async';
import 'dart:convert';
import 'dart:html';

ButtonElement addItem;
InputElement toDoInput;
DivElement toDoList;
ButtonElement deleteAll;
CheckboxInputElement removeItem;
Element element;
Element msg;
ButtonElement btnAutenticar;
ButtonElement btnCadastrar;
ButtonElement btnNovoUsuario;
ButtonElement btnVoltarLogin;
//final url = 'https://jsonplaceholder.typicode.com/posts';
final url = 'http://localhost:8888';
String token = '';

void main() {
  toDoInput = querySelector('#to-do-input');
  toDoList = querySelector('#to-do-list');
  addItem = querySelector('#add');
  removeItem = querySelector('#remove-item');
  btnAutenticar = querySelector('#autenticar');
  btnCadastrar = querySelector('#cadastrar');
  btnNovoUsuario = querySelector('#novo-usuario');
  btnVoltarLogin = querySelector('#voltar-login');
  addItem.onClick.listen(addToDoItem);
  btnAutenticar.onClick.listen(autenticar);
  btnCadastrar.onClick.listen(create);
  btnNovoUsuario.onClick.listen(enableFormCreate);
  btnVoltarLogin.onClick.listen(enableFormLogin);
  removeItem.onClick.listen(removeItemFromList);
  enableDisableElements();
}

void enableDisableElements() {
  querySelector('#form').style.display = 'none';
  querySelector('#create').style.display = 'none';
  querySelector('#msg').style.display = 'none';
  querySelector('#msg-success').style.display = 'none';
}

void enableFormCreate(Event e) {
  querySelector('#login').style.display = 'none';
  querySelector('#form').style.display = 'none';
  querySelector('#create').style.display = 'block';
}

void enableFormLogin(Event e) {
  querySelector('#login').style.display = 'block';
  querySelector('#form').style.display = 'none';
  querySelector('#create').style.display = 'none';
}

Future<void> init() async {
  try {
    //final jsonString = await HttpRequest.getString('$url/todo');
    await HttpRequest.request('$url/todo', method: 'GET', requestHeaders: {
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': '$token'
    }).then((resp) {
      var response = json.decode(resp.responseText);
      print('response.length ${response.length}');
      if (response.length > 0) {
        processaResponse(response);
      }
    });
  } catch (e) {
    print('Couldn\'t open $url');
    print(e);
  }
}

void processaResponse(result) {
  for (final prop in result) {
    var element = Element.html('''
      <div class="media text-muted pt-3">
        <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
          <strong class="d-block text-gray-dark"></strong>
          <input type="checkbox" id="remove-item" value="${prop['id']}"/>
          ${prop['name']}
        </p>
      </div>
    ''');
    toDoList.children.add(element);
  }
}

void addToDoItem(Event e) {
  if (toDoInput.value == '') {
    window.alert('Não é permitidido informar um item em branco para lista.');
    return;
  }
  post(toDoInput.value, 1);
}

Future<void> post(String newItem, int userId) async {
  Map<String, dynamic> data = {
    'name': newItem,
    'done': true
  };
  await HttpRequest.request('$url/todo',
      method: 'POST',
      sendData: json.encode(data),
      requestHeaders: {
        'Content-Type': 'application/json; charset=UTF-8',
        'authorization': '$token'
      }).then((HttpRequest resp) => {
        toDoList.children.add(Element.html('''
              <div class="media text-muted pt-3">
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                  <strong class="d-block text-gray-dark"></strong>
                  <input type="checkbox" id="remove-item" value=""/>
                  ${newItem}
                </p>
              </div>
            '''))
      });
}

void removeItemFromList(Event e) {
  print(removeItem);
}

void create(Event e) {
  createUser();
}


Future<void> createUser() async {
  InputElement name = querySelector('#name');
  InputElement email = querySelector('#email');
  InputElement password = querySelector('#password');
  print(email.value);
  print(password.value);
  await HttpRequest.request('$url/user',
          method: 'POST',
          sendData: json.encode({
            'name': name.value,
            'email': email.value,
            'password': password.value
          }),
          requestHeaders: {'Content-Type': 'application/json; charset=UTF-8'})
      .then((HttpRequest resp) {
    var response = json.decode(resp.responseText);
    if (response['email'] != '') {
      querySelector('#msg-success').style.display = 'block';
      querySelector('#msg-success').text = 'Usuário criado com sucesso.';
    }
  });
}

void autenticar(Event e) {
  realizarLogin();
}

Future<void> realizarLogin() async {
  InputElement email = querySelector('#inputEmail');
  InputElement password = querySelector('#inputPassword');
  await HttpRequest.request('$url/session',
      method: 'POST',
      sendData: json.encode({'email': email.value, 'password': password.value}),
      requestHeaders: {
        'Content-Type': 'application/json; charset=UTF-8'
      }).then((resp) {
    var response = json.decode(resp.responseText);
    if (response['auth']) {
      token = response['token'];
      querySelector('#login').style.display = 'none';
      querySelector('#create').style.display = 'none';
      querySelector('#form').style.display = 'block';
      window.localStorage['user'] = json.encode(response);
      init();
    } else {
      querySelector('#msg').style.display = 'block';
      querySelector('#msg').text = 'Usuário inválido.';
    }
  });
}
