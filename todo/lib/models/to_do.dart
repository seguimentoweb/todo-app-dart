import 'package:todo/todo.dart';
import 'package:todo/models/user.dart';

class ToDo extends ManagedObject<_ToDo> implements _ToDo {}

/*
 * Class modelo
 */
class _ToDo {
  @primaryKey
  int id;
  String name;
  bool done = true;
  @Relate(#toDo)
  User user;
}
