import 'package:todo/models/to_do.dart';
import 'package:todo/models/user.dart';
import 'package:todo/todo.dart';

class ToDoController extends ResourceController {
  // Metodo construtor
  ToDoController(this.context) {
    acceptedContentTypes = [ContentType.json];
  }

  final ManagedContext context;

  /*
   * Lista teste
   */
  final List<ToDo> toDos = [
    ToDo()
      ..id = 1
      ..name = 'Trabalho da faculdade'
      ..done = true,
    ToDo()
      ..id = 2
      ..name = 'Compras'
      ..done = true,
    ToDo()
      ..id = 3
      ..name = 'Jantar'
      ..done = false,
    ToDo()
      ..id = 4
      ..name = 'Trabalhar'
      ..done = false,
  ];

  /*
   * Metodo retorna todos os itens da lista
   * Future equivale a uma Promise no javascript
   * Aguarda a execução até o final antes de retornar os dados
   */
  @Operation.get()
  Future<Response> getAllToDos() async {
    final User user = request.attachments['user'] as User;
    final query = Query<ToDo>(context)
      ..where((todo) => todo.user.id).equalTo(user.id);
    final toDos = await query.fetch();
    return Response.ok(toDos);
  }

  /*
   * Metodo que retorna um item da lista, pegando pelo o id
   * passado na requisição
   */
  @Operation.get('id')
  Future<Response> getToDoByID() async {
    final User user = request.attachments['user'] as User;
    final id = int.parse(request.path.variables['id']);
    final query = Query<ToDo>(context)
      ..where((todo) => todo.id).equalTo(id)
      ..where((todo) => todo.user.id).equalTo(user.id);
    final toDo = await query.fetchOne();

    if (toDo == null) {
      return Response.notFound();
    }

    return Response.ok(toDo);
  }

  /*
   * Metodo que adiciona um item na lista de toDos
   */
  @Operation.post()
  Future<Response> postToDo(@Bind.body() ToDo toDo) async {
    final User user = request.attachments['user'] as User;
    final body = ToDo()..read(await request.body.decode(), ignore: ['id']);
    final query = Query<ToDo>(context)
      ..values.name = body.name
      ..values.done = body.done
      ..values.user.id = user.id;
    final toDo = await query.insert();
    return Response.ok(toDo);
  }

  /*
   * Metodo que remove um item da lista e atualizar a mesma
   */
  @Operation.put('id')
  Future<Response> putToDo(@Bind.path('id') int id) async {
    final User user = request.attachments['user'] as User;
    final body = ToDo()..read(await request.body.decode(), ignore: ['id']);
    final query = Query<ToDo>(context)
      ..values = body
      ..where((todo) => todo.id).equalTo(id)
      ..where((todo) => todo.user.id).equalTo(user.id);
    final toDo = await query.updateOne();
    return Response.ok(toDo);
  }

  /*
   * Metodo que remove um item da lista a partir de um index
   */
  @Operation.delete('id')
  Future<Response> deleteToDoByID(@Bind.path('id') int id) async {
    final User user = request.attachments['user'] as User;
    final query = Query<ToDo>(context)
      ..where((todo) => todo.id).equalTo(id)
      ..where((todo) => todo.user.id).equalTo(user.id);
    await query.delete();
    return Response.ok({'ok': true});
  }
}
