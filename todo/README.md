# Documentacao oficial do aqueduct: https://aqueduct.io/

# Instalando o aqueduct, rode o comando abaixo

pub global activate aqueduct

# Criando um novo projeto com o aqueduct

aqueduct create todo

# Executando o servidor aqueduct

aqueduct serve

# Criando migrations a partir do model ja criado
# Com esse comando, o Aqueduct detecta as models do
# projeto para criar a migração, as migrações ficam
# dentro do diretório migrations

aqueduct db generate

# Executando a migrantion

aqueduct db upgrade