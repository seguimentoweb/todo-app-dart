# todo app dart

# Requisitos obrigatórios
1. - Instalar o SDK do Dart. [Clique aqui](https://dart.dev/get-dart)
2. - Instalar as dependências conforma abaixo:
    - pub global activate aqueduct
    - pub global activate webdev
    - pub global activate stagehand
3. - Ter o postgres instalado

# Passos para executar o projeto web
1. - Acesse a pasta todo-web
2. - Execute o comando: pub get. Isso irá instalar as dependências do projeto
3. - Execute o comando: webdev serve
Obs: Ao executar o comando acima o projeto web irá rodar na porta 8080 (http://localhost:8080)

# Passos para executar o projeto api
1. - Acesse a pasta todo
2. - Execute o comando: pub get. Isso irá instalar as dependências do projeto
3. - Execute o comando: aqueduct db upgrade. Isso irá executar as migrations para criar as tabelas
4. - Execute o comando: aqueduct serve
Obs: Ao executar o comando acima o projeto web irá rodar na porta 8080 (http://localhost:8888)

# Próximos passos
1. - Realizar um cadastro para criar o seu usuário para acessar o sistema.

